# PPY1 2024

## ⚠️⚠️⚠️ In case your time table does not allow you to attend the lectures ⚠️⚠️⚠️

We are recording the lectures on Teams, and you will have access to the recordings, if this is your situation. Please let me know why on matej.mojzes@fjfi.cvut.cz, and I will grant you access.

Otherwise, it is mandatory (and much more fun!) to attend the lecture live. Thanks :)


### In this repository you can find:

* whatever we are looking at during lectures, see the `lecture_<lecture_number>.ipynb` Jupyter notebooks, especially in the first one you can find the criteria for "zápočet" (tl;dr it is an individual project which needs to cover at least three topics covered during lectures)
* student projects will be added to the `projects/` folder over time (via merge requests)
